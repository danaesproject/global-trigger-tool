# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_gttMainWindow(object):
    def setupUi(self, gttMainWindow):
        gttMainWindow.setObjectName(_fromUtf8("gttMainWindow"))
        gttMainWindow.resize(796, 587)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(gttMainWindow.sizePolicy().hasHeightForWidth())
        gttMainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QtGui.QWidget(gttMainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.spreadsheetDirLabel = QtGui.QLabel(self.centralwidget)
        self.spreadsheetDirLabel.setGeometry(QtCore.QRect(14, 22, 137, 16))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spreadsheetDirLabel.sizePolicy().hasHeightForWidth())
        self.spreadsheetDirLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        self.spreadsheetDirLabel.setFont(font)
        self.spreadsheetDirLabel.setObjectName(_fromUtf8("spreadsheetDirLabel"))
        self.spreadsheetDirBox = QtGui.QLineEdit(self.centralwidget)
        self.spreadsheetDirBox.setGeometry(QtCore.QRect(170, 20, 481, 21))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spreadsheetDirBox.sizePolicy().hasHeightForWidth())
        self.spreadsheetDirBox.setSizePolicy(sizePolicy)
        self.spreadsheetDirBox.setObjectName(_fromUtf8("spreadsheetDirBox"))
        self.browseButton = QtGui.QPushButton(self.centralwidget)
        self.browseButton.setGeometry(QtCore.QRect(666, 16, 113, 32))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.browseButton.sizePolicy().hasHeightForWidth())
        self.browseButton.setSizePolicy(sizePolicy)
        self.browseButton.setObjectName(_fromUtf8("browseButton"))
        self.outputWindow = QtGui.QTextEdit(self.centralwidget)
        self.outputWindow.setGeometry(QtCore.QRect(12, 68, 771, 455))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.outputWindow.sizePolicy().hasHeightForWidth())
        self.outputWindow.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Courier New"))
        font.setPointSize(11)
        font.setBold(False)
        font.setWeight(50)
        self.outputWindow.setFont(font)
        self.outputWindow.setStyleSheet(_fromUtf8("table {\n"
                                                  "    font-family: \'Helvetica Neue\';\n"
                                                  "    border-collapse: collapse;\n"
                                                  "    width: 100%;\n"
                                                  "}\n"
                                                  "\n"
                                                  "td, th {\n"
                                                  "    border: 1px solid #dddddd;\n"
                                                  "    text-align: left;\n"
                                                  "    padding: 8px;\n"
                                                  "}\n"
                                                  "\n"
                                                  "tr:nth-child(even) {\n"
                                                  "    background-color: #dddddd;\n"
                                                  "}"))
        self.outputWindow.setObjectName(_fromUtf8("outputWindow"))
        self.outputWindowLabel = QtGui.QLabel(self.centralwidget)
        self.outputWindowLabel.setGeometry(QtCore.QRect(16, 48, 759, 16))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.outputWindowLabel.sizePolicy().hasHeightForWidth())
        self.outputWindowLabel.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        font.setBold(True)
        font.setWeight(75)
        self.outputWindowLabel.setFont(font)
        self.outputWindowLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.outputWindowLabel.setObjectName(_fromUtf8("outputWindowLabel"))
        self.printButton = QtGui.QPushButton(self.centralwidget)
        self.printButton.setGeometry(QtCore.QRect(666, 530, 113, 32))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.printButton.sizePolicy().hasHeightForWidth())
        self.printButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        self.printButton.setFont(font)
        self.printButton.setObjectName(_fromUtf8("printButton"))
        self.saveButton = QtGui.QPushButton(self.centralwidget)
        self.saveButton.setGeometry(QtCore.QRect(546, 530, 113, 32))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.saveButton.sizePolicy().hasHeightForWidth())
        self.saveButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        self.saveButton.setFont(font)
        self.saveButton.setObjectName(_fromUtf8("saveButton"))
        self.quitButton = QtGui.QPushButton(self.centralwidget)
        self.quitButton.setGeometry(QtCore.QRect(16, 530, 113, 32))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.quitButton.sizePolicy().hasHeightForWidth())
        self.quitButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        self.quitButton.setFont(font)
        self.quitButton.setObjectName(_fromUtf8("quitButton"))
        self.ProcessButton = QtGui.QPushButton(self.centralwidget)
        self.ProcessButton.setGeometry(QtCore.QRect(136, 530, 113, 32))
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ProcessButton.sizePolicy().hasHeightForWidth())
        self.ProcessButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        self.ProcessButton.setFont(font)
        self.ProcessButton.setObjectName(_fromUtf8("ProcessButton"))
        gttMainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(gttMainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 796, 22))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        gttMainWindow.setMenuBar(self.menubar)
        self.actionCreate_Config_File = QtGui.QAction(gttMainWindow)
        self.actionCreate_Config_File.setObjectName(_fromUtf8("actionCreate_Config_File"))
        self.actionRead_Config_File = QtGui.QAction(gttMainWindow)
        self.actionRead_Config_File.setObjectName(_fromUtf8("actionRead_Config_File"))
        self.actionAbout = QtGui.QAction(gttMainWindow)
        self.actionAbout.setVisible(False)
        self.actionAbout.setIconVisibleInMenu(False)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.actionHelp_Docs = QtGui.QAction(gttMainWindow)
        self.actionHelp_Docs.setObjectName(_fromUtf8("actionHelp_Docs"))
        self.menuFile.addAction(self.actionCreate_Config_File)
        self.menuFile.addAction(self.actionRead_Config_File)
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionHelp_Docs)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(gttMainWindow)
        QtCore.QMetaObject.connectSlotsByName(gttMainWindow)

    def retranslateUi(self, gttMainWindow):
        gttMainWindow.setWindowTitle(_translate("gttMainWindow", "Global Trigger Tool", None))
        self.spreadsheetDirLabel.setText(_translate("gttMainWindow", "Spreadsheet Directory:", None))
        self.browseButton.setText(_translate("gttMainWindow", "Browse", None))
        self.outputWindowLabel.setText(_translate("gttMainWindow", "Results", None))
        self.printButton.setText(_translate("gttMainWindow", "Print", None))
        self.saveButton.setText(_translate("gttMainWindow", "Save", None))
        self.quitButton.setText(_translate("gttMainWindow", "Quit", None))
        self.ProcessButton.setText(_translate("gttMainWindow", "Process", None))
        self.menuFile.setTitle(_translate("gttMainWindow", "File", None))
        self.menuHelp.setTitle(_translate("gttMainWindow", "Help", None))
        self.actionCreate_Config_File.setText(_translate("gttMainWindow", "Create Config File", None))
        self.actionRead_Config_File.setText(_translate("gttMainWindow", "Read Config File", None))
        self.actionAbout.setText(_translate("gttMainWindow", "About", None))
        self.actionHelp_Docs.setText(_translate("gttMainWindow", "Help", None))
