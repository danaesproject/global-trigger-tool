# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_about(object):
    def setupUi(self, about):
        about.setObjectName(_fromUtf8("about"))
        about.resize(400, 300)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(about.sizePolicy().hasHeightForWidth())
        about.setSizePolicy(sizePolicy)
        self.aboutHeader = QtGui.QLabel(about)
        self.aboutHeader.setGeometry(QtCore.QRect(4, 20, 393, 16))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.aboutHeader.setFont(font)
        self.aboutHeader.setAlignment(QtCore.Qt.AlignCenter)
        self.aboutHeader.setObjectName(_fromUtf8("aboutHeader"))
        self.label = QtGui.QLabel(about)
        self.label.setGeometry(QtCore.QRect(4, 282, 393, 16))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.aboutDescription = QtGui.QLabel(about)
        self.aboutDescription.setGeometry(QtCore.QRect(4, 74, 391, 35))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Helvetica"))
        font.setPointSize(12)
        self.aboutDescription.setFont(font)
        self.aboutDescription.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.aboutDescription.setWordWrap(True)
        self.aboutDescription.setObjectName(_fromUtf8("aboutDescription"))

        self.retranslateUi(about)
        QtCore.QMetaObject.connectSlotsByName(about)

    def retranslateUi(self, about):
        about.setWindowTitle(_translate("about", "About", None))
        self.aboutHeader.setText(_translate("about", "Global Trigger Tool Application", None))
        self.label.setText(_translate("about", "2016 Danae Wardrup", None))
        self.aboutDescription.setText(_translate("about",
                                                 "A tool to help users in Patient Safety and Accreditation analyze Global Trigger Tool data worksheets.",
                                                 None))
