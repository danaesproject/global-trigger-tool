"""
Contains allowable cell values. These may be changed to whatever is necessary.

The value before the colon (this value is called the 'key') is the actual trigger code. The value after the colon
is a description that may be used in the final output. The trigger and event codes in each spreadsheet are checked
against the relevant keys to ensure data integrity. Thus, it is extremely important that these key values are
double-checked for errors. Any errors in this file will render the tool useless.

Changing the value in either dictionary will only affect the tool output. You may edit the values as you wish.

'KEY': VALUE

#TODO: Replace None in adverse_event_dictionary with actual values
"""

# List of valid triggers
trigger_dictionary = {
    # CARE MODULE TRIGGERS
    'C1': 'Transfusion or use of blood products',
    'C2': 'code/arrest/rapid response team',
    'C3': 'Acute dialysis',
    'C4': 'Positive blood culture',
    'C5': 'X-ray or Doppler studies for emboli or DVT',
    'C6': 'Decrease of greater than 25%% in hemoglobin or hematocrit',
    'C7': 'Patient fall',
    'C8': 'Pressure ulcers',
    'C9': 'Readmission within 30 days',
    'C10': 'Restraint use',
    'C11': 'Healthcare-associated infection',
    'C12': 'In-hospital stroke',
    'C13': 'Transfer to higher level of care',
    'C14': 'Any procedure complication',
    'C15': 'Other',

    # SURGICAL MODULE TRIGGERS
    'S1': 'Return to surgery',
    'S2': 'Change in procedure',
    'S3': 'Admission to intensive care post-op',
    'S4': 'Intubation/reintubation/BiPap in Post Anasthesia Care Unit (PACU)',
    'S5': 'X-ray intra-op or in PACU',
    'S6': 'Intra-op or post-op death',
    'S7': 'Mechanical venilation greater than 24 hours post-op',
    'S8': 'Intra-op epinephrine, norepinephrine, naloxone, or romazicon',
    'S9': 'Post-op troponin level greater than 1.5 ng/ml',
    'S10': 'Injury, repair or removal of organ',
    'S11': 'Any operative complication',

    # MEDICATION MODULE TRIGGERS
    'M1': 'Clostridium difficile positive stool',
    'M2': 'Partial thromboplastin time greater than 100 seconds',
    'M3': 'International Normalized Ratio (INR) greater than 6',
    'M4': 'Glucose less than 50 mg/dl',
    'M5': 'Rising BUN or serum creatinine greater than 2 times baseline',
    'M6': 'Vitamin K administration',
    'M7': 'Benadryl (Diphenhydramine) use',
    'M8': 'Romazicon (Flumazenil) use',
    'M9': 'Naloxone (Narcan) use',
    'M10': 'Anti-emetic use',
    'M11': 'Over-sedation/hypotension',
    'M12': 'Abrupt medication stop',
    'M13': 'Other',

    # INTENSIVE CARE MODULE TRIGGERS
    'I1': 'Pneumonia onset',
    'I2': 'Readmission to intensive care',
    'I3': 'In-unit procedure',
    'I4': 'Intubation/reintubation',

    # PERINATAL MODULE TRIGGERS
    'P1': 'Terbatuline use',
    'P2': '3rd or 4th-degree lacerations',
    'P3': 'Platelet count less than 50,000',
    'P4': 'Estimated blood loss > 500ml (vaginal) or > 1,000 ml (C-section)',
    'P5': 'Speciality consult',
    'P6': 'Oxytocic agents',
    'P7': 'Intrumented delivery',
    'P8': 'General anasthesia',

    # EMERGENCY DEPT MODULE TRIGGERS
    'E1': 'Readmission to ED within 48 hours',
    'E2': 'Time in ED greater than 6 hours'
}

ae_type_category_dictionary = {
    'Medication': None,
    'Infection': None,
    'Equipment': None,
    'Procedural': None,
    'Falls': None,
    'Lab/Diagnostic': None,
    'Other': None
}

ae_harm_dictionary = {
    'E': 'Temporary harm to the patient and required intervention',
    'F': 'Temporary harm to the patient and required initial or prolonged hospitalizaiton',
    'G': 'Permenant patient harm',
    'H': 'Intervention required to sustain life',
    'I': 'Patient Death'
}
