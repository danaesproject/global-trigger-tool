#!/bin/bash

compileFile=$1  # File to compile
outName=$2  # Output filename
outLocation=$3  # Output directory name
log_file="PyInstaller.txt"

if [[ -n "$compileFile" ]] && [[ -n "$outLocation" ]] && [[ -n "$outLocation" ]]; then
        echo "$1=$( date +%s )" >> ${log_file}
        . /home/rwardrup/DEV/venv_wine/bin/activate
        #wine c:/
        #wine c:/Python34/python.exe /home/rwardrup/DEV/pyinstaller/pyinstaller.py --console --debug --noupx --hidden-import xlrd $1 -n gtt-$2 --distpath $3
        wine c:/Python34/python.exe /home/rwardrup/DEV/pyinstaller/pyinstaller.py gtt.spec
else
    echo "argument error"

fi