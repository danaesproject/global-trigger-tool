"""
Global Trigger Calculation Tool
Danae Wardrup - 2016

A tool to calculate statistics using global trigger spreadsheets
"""

import datetime
import os
import platform
import subprocess
import sys
from itertools import product

import configobj
import xlrd
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import letter, portrait
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer
from reportlab.platypus.flowables import KeepTogether
from tabulate import tabulate

import stoplist.hugeList as stopwords
from config import dictionaries

current_path = os.path.dirname(os.path.realpath(__file__))
default_config_path = os.path.join(current_path + '/gtt_config.txt')

valid_ae_value = dictionaries.ae_type_category_dictionary
valid_harm_score_value = dictionaries.ae_harm_dictionary
valid_trigger_value = dictionaries.trigger_dictionary

stopwords = stopwords.bad_word

global error_status
global config

# A few GUI options
PdfRow1Color = colors.whitesmoke  # This and the following line denote the colors for alternating PDF tables
PdfRow2Color = colors.lightblue
table_formats = 'fancy_grid'  # This denotes the type of table in the Qt window
font = "Helvetica 13"  # The font for the Qt window
background = 'white smoke'  # Qt window color
buttonPadding = 65  # Space on either side of bottons
valid_path = False

NA_list = ['NONE', 'N/A', 'UA', 'U/A', 'NA']  # Do not use a blank character in this list, ie ' '.

from gui.gui import *

class GTT(QtGui.QMainWindow, Ui_gttMainWindow):
    results_output = None
    configPath = None
    internal_config = True
    testMode = True

    def __init__(self, testMode):
        self.testMode = testMode

        if not self.testMode:
            QtGui.QMainWindow.__init__(self)
            Ui_gttMainWindow.__init__(self)
            self.setupUi(self)

            # Button actions
            self.quitButton.clicked.connect(self.close)  # quit button
            self.ProcessButton.clicked.connect(self.gui_process)  # Process button
            self.browseButton.clicked.connect(self.browse_directory)  # browse button
            self.saveButton.clicked.connect(self.output_file)  # save button
            self.printButton.clicked.connect(self.printer)  # print button

            self.actionCreate_Config_File.triggered.connect(self.createConfig)  # create config
            self.actionRead_Config_File.triggered.connect(self.readConfig)  # read config

            self.actionHelp_Docs.triggered.connect(self.helpDisplay)  # read config
            # self.actionAbout.triggered.connect(self.popupAbout)  # read config

            # Make sure buttons aren't enabled before data is loaded
            self.ProcessButton.setEnabled(False)
            self.saveButton.setEnabled(False)
            self.printButton.setEnabled(False)

            self.spreadsheetDirBox.setReadOnly(True)  # No typing in the output window!

            self.setFixedSize(self.size())  # Eventually would be nice to allow window resizing

    def word_count(self, adverse_events_freetxt):
        """
        Create word count
        :return: dictionary where keys are words and values are counts
        """
        word_count_dict = {}
        self.word_count_list = []
        filtered_words = [word for word in adverse_events_freetxt if word not in stopwords]

        for word in filtered_words:
            word_count_dict[word] = filtered_words.count(word)

        for word, count in word_count_dict.items():
            temp_tuple = (word.title(), count)
            self.word_count_list.append(temp_tuple)

        self.word_count_list.sort(key=lambda x: float(x[1]), reverse=True)
        print(self.word_count_list)
        return self.word_count_list

    def gui_process(self):
        """
        This runs the entire process when the 'Process' button is pressed.
        :return: magic
        """

        _tppv_results = []
        _harm_results = []
        _ae_results = []
        _trigger_counts = []
        _trigger_type_counts = []

        FileList = SheetLister(path)  # Get paths to all files in root dir and subdirectories.
        error, error_list = self.check_all_files(path, FileList, False)  # Validate data report any errors

        if not error:
            # Import all the columns if data has been successfully validated
            all_triggers, ind_triggers_with_harm, harm_categories, ae_type, import_error, total_admissions, \
            all_adverse_events, all_harm_events, outside_event_count, adverse_events_freetxt = self.importer(FileList)

            # Give them something to look at if analyzing a bunch of data
            self.outputWindow.setText("Successfully imported {0} spreadsheets. Now crunching numbers..."
                                      .format(len(FileList)))

            # Calculate the stats
            self.tppv_results = tppv(all_triggers, ind_triggers_with_harm, False)
            self.harm_results = harm_categ_distr_table(harm_categories)
            self.ae_type_result = ae_distr_table(ae_type)
            self.ae_word_count = self.word_count(adverse_events_freetxt)
            self.trigger_counts, self.trigger_type_counts = all_trigger_count(all_triggers)

            # Create tuples with rounded percentages in this format: (name, count, percentage)
            for item in self.tppv_results:
                percentage = '{0:.2f}'.format(item[2]) + '%'
                row_tuple = (item[0], item[1], percentage)
                _tppv_results.append(row_tuple)

            for item in self.harm_results:
                percentage = '{0:.2f}'.format(item[2]) + '%'
                row_tuple = (item[0], item[1], percentage)
                _harm_results.append(row_tuple)

            for item in self.ae_type_result:
                percentage = '{0:.2f}'.format(item[2]) + '%'
                row_tuple = (item[0], item[1], percentage)
                _ae_results.append(row_tuple)

            for item in self.trigger_counts:
                if item[2] < 0.01:  # Sometimes the percentages are low, so print a nice output if that's the case.
                    percentage = '< 0.01%'
                else:
                    percentage = '{0:.2f}'.format(item[2]) + '%'
                row_tuple = (item[0], item[1], percentage)
                _trigger_counts.append(row_tuple)

            for item in self.trigger_type_counts:
                if item[2] < 0.01:
                    percentage = '< 0.01%'
                else:
                    percentage = '{0:.2f}'.format(item[2]) + '%'
                row_tuple = (item[0], item[1], percentage)
                _trigger_type_counts.append(row_tuple)

            # Build the tables
            tppv_table = tabulate(_tppv_results, headers=["Trigger", "Trigger with Harm Count", "Percentage"],
                                  floatfmt='.2f',
                                  tablefmt=table_formats)

            harm_table = tabulate(_harm_results, headers=["Harm Category", "Count", "Percentage"],
                                  floatfmt='.2f', tablefmt=table_formats)

            ae_table = tabulate(_ae_results,
                                headers=["Adverse Event Type", "Count", "Percentage"],
                                floatfmt='.2f', tablefmt=table_formats)

            word_count_table = tabulate(self.ae_word_count, headers=["Word", "Count"], tablefmt=table_formats)

            trigger_count_table = tabulate(_trigger_counts, headers=["Trigger", "Count", "Percent"],
                                           floatfmt='.2f', tablefmt=table_formats)

            trigger_type_count_table = tabulate(_trigger_type_counts, headers=["Trigger Module", "Count", "Percentage"],
                                                floatfmt='.2f', tablefmt=table_formats)

            # Start appending the tables to a loooong string
            self.results_output = "Trigger Table\n" + \
                                  trigger_count_table + "\n\n" + \
                                  "Trigger Module Table\n" + \
                                  trigger_type_count_table + "\n\n"

            # Add results strings together to display all at the same time in output
            self.results_output += "Trigger-Positive Predictive Value Table\n" + \
                                   tppv_table + "\n\n" + \
                                  "Harm Category Distribution Table\n" + \
                                  harm_table + "\n\n" + \
                                  "Adverse Event Type Distribution Table\n" + \
                                  ae_table + '\n\n'
            # "Adverse Event Free Text Word Count\n" + word_count_table + '\n\n'

            self.ae_per_100 = "\n\nAdverse Events per 100 Admissions: {}".format(
                ae_to_admission_rate(all_adverse_events, total_admissions))
            self.harm_per_100 = "\n\nHarmful Events per 100 Admissions: {}".format(
                harm_to_admission_rate(all_harm_events, total_admissions))
            self.outside_events = "\n\nPercentage of Events that Occurred Prior to Admission: {}%".format(
                preadmit_event_rate(outside_event_count, total_admissions))

            self.results_output += self.ae_per_100

            self.results_output += self.harm_per_100

            self.results_output += self.outside_events

            # Show them if they used their config file or not
            if self.internal_config:
                self.results_output += '\n\n**Using default internal column definitions**'
            elif not self.internal_config:
                self.results_output += '\n\n**Using user-defined column definitions**'

            # FInally, update the output window
            self.outputWindow.setText(self.results_output)
            # self.outputWindow.setEnabled(False)
            self.outputWindow.setReadOnly(True)
            self.outputWindow.verticalScrollBar()

        else:  # Otherwise, if there were any errors, print them to the window
            errors = ''
            error_list = list(set(error_list))
            for i in error_list:
                errors += i
                self.outputWindow.setText(errors)
            self.outputWindow.setReadOnly(True)

    def browse_directory(self):
        """
        Browse for spreadsheet directory
        :return: Add chosen directory to text entry bar
        """

        global path  # Make path a global variable that can be accessed everywhere
        global valid_path

        # Create prompt window for directory
        if self.testMode == False:
            path = QtGui.QFileDialog.getExistingDirectory(self, "Select Spreadsheet Directory")

        # If directory contains no spreadsheets, return an error message.
        if len(SheetLister(path)) == 0:
            if self.testMode == False:
                self.outputWindow.setText('Error: no spreadsheets found in directory')
            valid_path = False
            error_status = True

            # Disable process button
            if self.testMode == False:
                self.ProcessButton.setEnabled(False)
                self.printButton.setEnabled(False)
                self.saveButton.setEnabled(False)

        # If directory does contain spreadsheets, enable the process button and add the directory path to the text
        # entry bar.
        else:
            if self.testMode == False:
                # Once user enters valid directory, enable process button
                self.ProcessButton.setEnabled(True)
                self.printButton.setEnabled(True)
                self.saveButton.setEnabled(True)
                self.spreadsheetDirBox.setText(path)

            # Reset text entry bar, but add the directory path
            # Label(text="Spreadsheet Directory: ", font=font, bg=background).grid(row=1, column=0, sticky=W, padx=20)
            # tempPath = StringVar(GTT, value=path)
            # spreadsheetDir = Entry(GTT, textvariable=tempPath, width=60, state=DISABLED, font=font)
            #spreadsheetDir.grid(row=1, column=2, sticky=W)

            valid_path = True

            return path

    def check_if_config_exists(self, configPath):
        """
        Checks if configuration file exists
        :param configPath: Path to configuration file
        :return: Bool
        """

        if os.path.isfile(configPath):
            return True
        else:
            return False

    def readConfig(self):
        """
        Read configuration file if it exists
        :return: None
        """

        global config
        global error_status

        if self.testMode == False:
            self.configPath = QtGui.QFileDialog.getOpenFileName(self, 'Open Configuration File', '', 'TXT (*.txt)')

        # Create prompt window for directory
        config = configobj.ConfigObj(self.configPath)
        self.internal_config = False
        error_status = False

        return config

    def createConfig(self):
        """
        Create configuration file defining valid entries, column names, etc.
        :return: None
        """

        if self.testMode == False:
            dialog = QtGui.QFileDialog()
            dialog.setFilter(dialog.filter() | QtCore.QDir.Hidden)
            dialog.setDefaultSuffix('txt')
            dialog.setNameFilters(['TXT (*.txt)'])
            self.configPath = dialog.getSaveFileName(self, 'Configuration File Save Location', "", "TXT (*.txt)")

        if self.configPath:

            valid_triggers = valid_trigger_value.keys()
            valid_adverse_events = valid_ae_value.keys()
            valid_harm_categories = valid_harm_score_value.keys()

            triggers = []
            adverse_events = []
            harm_categories = []

            # Create lists of the various values
            for trigger in valid_triggers:
                triggers.append(trigger)

            for adverse_event in valid_adverse_events:
                adverse_events.append(adverse_event)

            for harm_category in valid_harm_categories:
                harm_categories.append(harm_category)

            # Sort lists
            triggers = sorted(triggers)
            adverse_events = sorted(adverse_events)
            harm_categories = sorted(harm_categories)

            # Intialize config object
            config = configobj.ConfigObj()
            config.filename = self.configPath

            # Build list of triggers
            config["Valid Triggers"] = triggers

            # Build list of adverse events
            config["Adverse Events"] = adverse_events

            # Build list of harm categories
            config["Harm Categories"] = harm_categories

            # Set header row
            config['Header Row'] = 2

            # Spreadsheet trigger columns
            config['Triggers (With AE) Column Name'] = 'Trigger(s) WITH associated AE'
            config['Triggers (Without AE) Column Name'] = 'Trigger(s) WITHOUT associated AE'

            # Spreadsheet harm category column
            config['Harm Category Column Name'] = 'Harm Category (if AE is harmful)'

            # Spreadsheet adverse event type column
            config['Adverse Event Type Column Name'] = 'Adverse Event Type'

            # Outside Event Column
            config['Outside Event Column Name'] = 'Outside Event Y or N'

            # Adverse event column
            config['Adverse Event Free Text Column'] = 'Adverse Event (AE)'

            # Write to file
            config.write()

    def output_file(self):
        """
        Set output txt file
        :return: A .txt file containing output text
        """

        output_text = self.results_output

        dialog = QtGui.QFileDialog()
        dialog.setFilter(dialog.filter() | QtCore.QDir.Hidden)
        dialog.setDefaultSuffix('pdf')
        dialog.setNameFilters(['PDF (*.pdf)'])
        if self.testMode == False:
            output_file_path = dialog.getSaveFileName(self, 'Report Output File Location', "", "PDF (*.pdf)")
        if len(output_file_path) > 0:
            # file = open(output_file_path, 'w')
            # file.write(output_text)
            # file.close()
            self.writePDF(output_file_path, output_text)

    def check_all_files(self, path, FileList, unitTest):
        """
        Scans all files and builds a list of those with errors
        :param path: path to files
        :param FileList: all files in path
        :return: text
        """

        self.error_list = []
        error_status = False
        invalid_column_names = False

        for file in FileList:
            row_counter = 0
            _path = file  # + '/' + file  # Take input path and add /filename.ext to it, to create the full path to the file
            if file.endswith('xlsx') or file.endswith('.xls') or file.endswith('.xlsm') and 'template' not in file:

                spreadsheet = xlrd.open_workbook(_path)  # identifies desired spreadsheet
                sheet = spreadsheet.sheet_by_index(0)  # specifies sheet to open within spreadsheet
                # Now, check column headings

                if self.internal_config:
                    try:
                        _, _ = find_column(sheet, 'Trigger(s) WITH associated AE')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Trigger(s) WITH associated AE' not found in spreadsheet \n{0}.\n\n".format(
                                _path))
                        error_status = True

                    try:
                        _, _ = find_column(sheet, 'Trigger(s) WITHOUT associated AE')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Trigger(s) WITHOUT associated AE' not found in spreadsheet \n{0}.\n\n".format(
                                _path))
                        error_status = True

                    try:
                        _, _ = find_column(sheet, 'Harm Category (if AE is harmful)')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Harm Category (if AE is harmful)' not found in spreadsheet \n{0}.\n\n".format(
                                _path))
                        error_status = True

                    try:
                        _, _ = find_column(sheet, 'Adverse Event Type')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Adverse Event Type' not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        _, _ = find_column(sheet, 'Outside Event Y or N')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Outside Event Y or N' not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        _, _ = find_column(sheet, 'Adverse Event (AE)')
                    except Exception:
                        self.error_list.append(
                            "Column named 'Adverse Event (AE)' not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                else:  # Using config file
                    try:
                        triggers_with_ae_col, header_row = find_column(sheet, config['Triggers (With AE) Column Name'])
                    except TypeError:
                        self.error_list.append(
                            "Triggers With AE Column not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        triggers_without_ae_col, _ = find_column(sheet, config['Triggers (Without AE) Column Name'])

                    except TypeError:
                        self.error_list.append(
                            "Triggers Without AE Column not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True
                    try:
                        harm_score_col, _ = find_column(sheet, config['Harm Category Column Name'])
                    except TypeError:
                        self.error_list.append("Harm Score Column not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        ae_type_col, _ = find_column(sheet, config['Adverse Event Type Column Name'])
                    except TypeError:
                        self.error_list.append("AE Type Column not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        outside_event_col, _ = find_column(sheet, config['Outside Event Column Name'])
                    except TypeError:
                        self.error_list.append("Outside Event Column not found in spreadsheet \n{0}.\n\n".format(_path))
                        error_status = True

                    try:
                        adverse_event_free_text_column, _ = find_column(sheet, config['Adverse Event Free Text Column'])
                    except TypeError:
                        self.error_list.append(
                            "Adverse Event Text column not found in spreadsheet\n{0}.\n\n".format(_path))
                        error_status = True

                if not error_status:
                    header_row, triggers_with_ae_col, triggers_without_ae_col, harm_score_col, ae_type_col, \
                    outside_event_col, adverse_event_free_text_column = self.grab_column_locations(sheet)

                    for row in range(sheet.nrows):
                        row_counter += 1
                        strip_white_space_list = []
                        _row_triggers_without_ae = []

                        if row > header_row:  # skipping rows 0 and 1 because those are for team member names and headers

                            row_triggers_with_ae = str(sheet.row(row)[triggers_with_ae_col].value)
                            try:
                                row_triggers_with_ae = row_triggers_with_ae.upper().replace(" ", "")

                            except AttributeError:
                                self.error_list.append("There is an incorrect Triggers With AE value in row {0}, file"
                                                       " {1}.".format(row, _path))
                                error_status = True
                            row_triggers_with_ae = row_triggers_with_ae.split(',')

                            row_triggers_without_ae = str(sheet.row(row)[triggers_without_ae_col].value)

                            try:
                                row_triggers_without_ae = row_triggers_without_ae.upper().replace(" ", "")
                            except AttributeError:
                                self.error_list.append("There is an incorrect Triggers Without AE value in row {0}, "
                                                       "file {1}.".format(row, _path))
                                error_status = True

                            # splits triggers in row by comma
                            row_triggers_without_ae = row_triggers_without_ae.split(',')

                            row_harm_score = sheet.row(row)[harm_score_col].value
                            try:
                                row_harm_score = row_harm_score.upper().replace(" ", "")
                            except AttributeError:
                                self.error_list.append("There is an incorrect Harm Category AE value in row {0}, "
                                                       "file {1}.".format(row, _path))
                                error_status = True
                            row_ae_type = sheet.row(row)[ae_type_col].value.title().replace(" ", "")

                            outside_event = sheet.row(row)[outside_event_col].value
                            try:
                                outside_event = outside_event.upper().replace(" ", "")
                            except AttributeError:
                                self.error_list.append("There is an incorrect Harm Category AE value in row {0}, "
                                                       "file {1}.".format(row, _path))
                                input("outside event error")
                                error_status = True

                            if outside_event in ['YES', 'Y'] and len(row_ae_type) == 0:
                                self.error_list.append(
                                    "Outside Adverse Event present with no Adverse Event Type assigned. "
                                    "Row {0}, File {1}.\n\n"
                                        .format(row_counter, _path))
                                error_status = True

                            if outside_event not in ['YES', 'Y', 'NO', 'N'] and outside_event not in NA_list \
                                    and len(outside_event) > 0:
                                self.error_list.append(
                                    "Invalid value ({0})in 'Outside Event Y or N' column in row {1}, "
                                                  "file {2}. \n\n"
                                    .format(outside_event, row_counter, _path))
                                print(outside_event)
                                error_status = True

                            if not check_harm_score(row_harm_score):
                                error_status = True
                                self.error_list.append(
                                    'Invalid harm category ({0}) found in row {1}, column {2}, file {3}.\n\n'
                                        .format(row_harm_score, row_counter, harm_score_col + 1, _path))  # force commit

                            if not check_ae_type(row_ae_type):
                                error_status = True
                                self.error_list.append(
                                    'Invalid adverse event type ({0}) found in row {1}, column {2}, file {3}.'
                                    '\n\n'.format(row_ae_type, row_counter, ae_type_col + 1, _path))

                            for trigger in row_triggers_with_ae:

                                if row_triggers_with_ae.count(trigger) > 1:
                                    self.error_list.append(
                                        "Trigger {0} appears in the Row Triggers With Adverse Event column more than "
                                        "once. Please remove the duplicate entry and try again.\nRow {1} in {2}\n\n"
                                            .format(trigger, row_counter, _path)
                                    )
                                    error_status = True

                            is_valid_ae_trigger, _, invalid_trigger = check_triggers(row_triggers_with_ae)

                            # Report error message if invalid triggers found
                            if not is_valid_ae_trigger:
                                error_status = True
                                self.error_list.append('Invalid trigger {0} found in row {1}, column {2}, file {3}.\n\n'
                                                       .format(invalid_trigger, row_counter,
                                                               triggers_with_ae_col + 1,
                                                               _path))

                            for trigger in row_triggers_without_ae:
                                if row_triggers_without_ae.count(trigger) > 1:
                                    self.error_list.append(
                                        "Trigger {0} appears in the Row Triggers Without Adverse Event column more than"
                                        " once. Please remove the duplicate entry and try again\nRow {1} in {2}\n\n"
                                            .format(trigger, row_counter, _path)
                                    )

                            is_valid_non_ae_trigger, _, invalid_trigger = check_triggers(row_triggers_without_ae)
                            if not is_valid_non_ae_trigger:
                                error_status = True
                                self.error_list.append('Invalid trigger {0} found in row {1}, column {2}, file {3}.\n\n'
                                                       .format(invalid_trigger, row_counter,
                                                               triggers_without_ae_col + 1,
                                                               _path))

                            if len(row_triggers_with_ae) > 0:
                                if len(row_harm_score) > 0:
                                    if row_triggers_with_ae[0] not in NA_list:
                                        if row_harm_score.upper() not in NA_list:
                                            if len(row_ae_type) == 0 or row_ae_type.upper() in NA_list:
                                                self.error_list.append(
                                                    "Harm category is present with empty adverse event type "
                                                    "cell.Row {0} in {1}.\n\n".format(row_counter, _path))
                                                error_status = True
                else:
                    invalid_column_names = True

        if invalid_column_names:
            self.error_list.append("\nYou must correct these issues before error checking can be performed on"
                                   " spreadsheet cell values.\n\n")

        if self.testMode:
            return error_status
        else:
            return error_status, self.error_list

    def grab_column_locations(self, sheet):
        """
        Get locations of columns depending on whether configuration file is used or not
        :return: Column locations
        """

        header_row = None
        triggers_with_ae_col = None
        triggers_without_ae_col = None
        harm_score_col = None
        ae_type_col = None
        outside_event_col = None
        adverse_event_free_text_column = None

        if self.internal_config:
            try:
                triggers_with_ae_col, header_row = find_column(sheet, 'Trigger(s) WITH associated AE')
            except Exception:
                self.error_list.append("Column named 'Trigger(s) WITH associated AE' not found in spreadsheet")
                error_status = True

            try:
                triggers_without_ae_col, _ = find_column(sheet, 'Trigger(s) WITHOUT associated AE')
            except Exception:
                self.error_list.append("Column named 'Trigger(s) WITHOUT associated AE' not found in spreadsheet")
                error_status = True

            try:
                harm_score_col, _ = find_column(sheet, 'Harm Category (if AE is harmful)')
            except Exception:
                self.error_list.append("Column named 'Harm Category (if AE is harmful)' not found in spreadsheet")
                error_status = True

            try:
                ae_type_col, _ = find_column(sheet, 'Adverse Event Type')
            except Exception:
                self.error_list.append("Column named 'Adverse Event Type' not found in spreadsheet")
                error_status = True

            try:
                outside_event_col, _ = find_column(sheet, 'Outside Event Y or N')
            except Exception:
                self.error_list.append("Column named 'Outside Event Y or N' not found in spreadsheet")
                error_status = True

            try:
                adverse_event_free_text_column = find_column(sheet, 'Adverse Event (AE)')
            except Exception:
                self.error_list.append("Column named 'Adverse Event (AE)' not found in spreadsheet")
                error_status = True

        else:
            try:  # TODO: Add the try/except code here.
                triggers_with_ae_col, header_row = find_column(sheet, config['Triggers (With AE) Column Name'])
                triggers_without_ae_col, _ = find_column(sheet, config['Triggers (Without AE) Column Name'])
                harm_score_col, _ = find_column(sheet, config['Harm Category Column Name'])
                ae_type_col, _ = find_column(sheet, config['Adverse Event Type Column Name'])
                outside_event_col, _ = find_column(sheet, config['Outside Event Column Name'])

                adverse_event_free_text_column, _ = find_column(sheet, config['Adverse Event Free Text Column'])

            except TypeError as e:
                self.outputWindow.setText('Error loading spreadsheet columns. Please check that they are '
                                          'valid, and create a configuration file to change the definitions'
                                          ' if necessary.')
                error_status = True

        return header_row, triggers_with_ae_col, triggers_without_ae_col, harm_score_col, ae_type_col, \
               outside_event_col, adverse_event_free_text_column

    def importer(self, FileList):
        """
        imports csv, xlsm or xlsx files found in directory
        :param FileList: list of strings describing the file names
        :return:
        """

        total_triggers = []
        ae_type = []
        harm_categ = []
        ind_triggers_with_harm = []  # List of lists containing all individual triggers assoc. with harm scores.
        counter = 1
        error_status = False
        total_admissions = 0
        all_adverse_events = 0
        all_harm_events = 0
        outside_event_count = 0
        self.error_list = []
        header_row = None
        total_adverse_events = []
        adverse_events_freetxt = []

        for file in FileList:  # for each file in a list of files
            row_counter = 1

            _path = file  #+ '/' + file  # Take input path and add /filename.ext to it, to create the full path to the file
            if file.endswith('xlsx') or file.endswith('.xls') or file.endswith('.xlsm') and 'template' not in file:
                try:
                    spreadsheet = xlrd.open_workbook(_path)  # identifies desired spreadsheet
                    sheet = spreadsheet.sheet_by_index(0)  # specifies sheet to open within spreadsheet

                    header_row, triggers_with_ae_col, triggers_without_ae_col, harm_score_col, ae_type_col, \
                    outside_event_col, adverse_event_free_text_column = self.grab_column_locations(sheet)

                    for row in range(sheet.nrows):
                        strip_white_space_list = []
                        _row_triggers_without_ae = []

                        if row > header_row:  # skipping rows 0 and 1 because those are for team member names and headers
                            total_admissions += 1

                            row_triggers_with_ae = sheet.row(row)[triggers_with_ae_col].value.upper().replace(" ", "")
                            row_triggers_with_ae = row_triggers_with_ae.split(',')

                            row_triggers_without_ae = sheet.row(row)[triggers_without_ae_col].value.upper().replace(" ",
                                                                                                                    "")
                            row_triggers_without_ae = row_triggers_without_ae.split(
                                ',')  # splits triggers in row by comma

                            row_harm_score = sheet.row(row)[harm_score_col].value.upper().replace(" ", "")
                            row_ae_type = sheet.row(row)[ae_type_col].value.title().replace(" ", "")

                            outside_event = sheet.row(row)[outside_event_col].value.upper().replace(" ", "")

                            if type(adverse_event_free_text_column) == tuple:
                                adverse_event_free_text_column = adverse_event_free_text_column[0]

                            row_adverse_events_text = sheet.row(row)[adverse_event_free_text_column].value.lower()
                            if len(row_adverse_events_text) > 0:
                                adverse_events_freetxt.append(row_adverse_events_text)

                            if outside_event not in ['N', 'NO']:
                                outside_event_count += 1

                            for i in row_triggers_with_ae:
                                if len(i) > 0:
                                    total_triggers.append(i.replace(" ", ""))

                            for i in row_triggers_without_ae:
                                if len(i) > 0:
                                    total_triggers.append(i.replace(" ", ""))
                                    _row_triggers_without_ae.append(i.replace(" ", ""))
                            row_triggers_without_ae = _row_triggers_without_ae

                            valid_ae_trigger, individual_trigger, _ = check_triggers(row_triggers_with_ae)
                            valid_non_ae_trigger, _, _ = check_triggers(row_triggers_without_ae)

                            if valid_ae_trigger and individual_trigger and row_ae_type not in NA_list and len(
                                    row_ae_type) > 0:
                                for i in row_triggers_with_ae:
                                    i = i.replace(" ", "")  # removes spaces from all trigger data
                                    if len(i) > 0 and i not in NA_list:
                                        strip_white_space_list.append(
                                            i)  # adds cleaned up triggers to list of row triggers

                                # adds all lists of row triggers to spreadsheet trigger list
                                if len(strip_white_space_list) > 0:
                                    ind_triggers_with_harm.append(strip_white_space_list)

                            # pulls and capitalizes all values from harm category column for each row
                            if len(row_harm_score) > 0 and row_harm_score not in NA_list:
                                # adds all lists of row harm scores to spreadsheet harm category list
                                harm_categ.append(row_harm_score)
                                all_harm_events += 1

                            if len(row_ae_type) > 0 and row_ae_type not in NA_list:
                                # adds all lists of row adverse events to spreadsheet harm category list
                                if row_ae_type == 'O':
                                    row_ae_type = 'Other'
                                if row_ae_type == 'M':
                                    row_ae_type = 'Medication'
                                if row_ae_type == 'P':
                                    row_ae_type = 'Procedural'
                                if row_ae_type == 'L/D':
                                    row_ae_type = 'Lab/Diagnostic'
                                if row_ae_type == 'I':
                                    row_ae_type = 'Infection'
                                if row_ae_type == 'E':
                                    row_ae_type = 'Equipment'
                                if row_ae_type == 'F':
                                    row_ae_type = 'Falls'
                                ae_type.append(row_ae_type)
                                all_adverse_events += 1

                        row_counter += 1

                    counter += 1

                except UnboundLocalError:
                    print("This is probably due to an incorrect column name specification. Check the columns and check"
                          "That macros aren't interfering with the names.")

        return total_triggers, ind_triggers_with_harm, harm_categ, ae_type, error_status, total_admissions, \
               all_adverse_events, all_harm_events, outside_event_count, adverse_events_freetxt

    def printer(self):
        """
        Sends text to the printer
        :return: Some paper with words on it
        """

        dialog = QtGui.QPrintDialog()
        if dialog.exec_() == QtGui.QDialog.Accepted:
            self.outputWindow.document().print_(dialog.printer())

    def writePDF(self, path, text):
        """
        Create PDF file from text
        :return:
        """

        # Get results into a format that reportlab can use
        harm_table = []
        trigger_table = []
        adverse_event_table = []
        word_count_table = []
        trigger_counts_table = []
        trigger_type_counts_table = []

        trigger_counts_table_title = 'Trigger Table'
        trigger_type_counts_table_title = 'Trigger Module Table'
        trigger_table_title = 'Trigger-Positive Predictive Value Table'
        trigger_table_comment = 'TPPV = count of independent trigger associated with harm\n' \
                                'divided by the number of times that trigger was identified'
        harm_table_title = 'Harm Category Distribution Table'
        adverse_event_table_title = 'Adverse Event Type Distribution Table'
        word_count_table_title = 'Adverse Event Free Text Word Count Table'

        trigger_counts_table_header = ['Trigger', 'Count', 'Percentage']
        trigger_type_table_header = ['Trigger', 'Count', 'Percentage']
        trigger_table_header = ['Trigger', 'Count', 'Percentage']
        harm_table_header = ['Harm Category', 'Count', 'Percentage']
        ae_table_header = ['Adverse Event Type', 'Count', 'Percentage']
        free_text_count_header = ['Word', 'Count']

        trigger_counts_table.append(trigger_counts_table_header)
        trigger_type_counts_table.append(trigger_type_table_header)
        trigger_table.append(trigger_table_header)
        harm_table.append(harm_table_header)
        adverse_event_table.append(ae_table_header)
        word_count_table.append(free_text_count_header)

        # Build the new tables so that reportlab can use them
        for row in self.trigger_counts:
            trigger = str(row[0])
            trigger_count = str(row[1])
            trigger_percentage = str(row[2])

            trigger_percentage = '{0:.2f}%'.format(float(trigger_percentage))
            new_row = [trigger, trigger_count, trigger_percentage]
            trigger_counts_table.append(new_row)

        for row in self.trigger_type_counts:
            trigger = str(row[0])
            trigger_count = str(row[1])
            trigger_percentage = row[2]

            trigger_percentage = '{0:.2f}%'.format(trigger_percentage)
            new_row = [trigger, trigger_count, trigger_percentage]
            trigger_type_counts_table.append(new_row)

        for row in self.tppv_results:
            trigger = str(row[0])
            trigger_count = str(row[1])
            trigger_percentage = row[2]

            trigger_percentage = '{0:.2f}%'.format(trigger_percentage)
            new_row = [trigger, trigger_count, trigger_percentage]
            trigger_table.append(new_row)

        for row in self.harm_results:
            harm_category = row[0]
            harm_count = row[1]
            harm_percentage = row[2]

            # Fix the decimals
            harm_percentage = '{0:.2f}%'.format(harm_percentage)
            new_row = [str(harm_category), str(harm_count), harm_percentage]
            harm_table.append(new_row)

        for row in self.ae_type_result:
            adverse_event = str(row[0])
            adverse_event_count = str(row[1])
            adverse_event_percentage = row[2]

            adverse_event_percentage = '{0:.2f}%'.format(adverse_event_percentage)
            new_row = [adverse_event, adverse_event_count, adverse_event_percentage]
            adverse_event_table.append(new_row)

        # Don't print a word_count table if it's empty
        if len(self.word_count_list) == 0:
            word_count_output = False
        else:
            word_count_output = True
            for row in self.word_count_list:
                word = row[0]
                word_count = str(row[1])

                new_row = [word, word_count]
                word_count_table.append(new_row)
                word_table_length = len(word_count_table)

        trigger_length = len(trigger_table)
        harm_length = len(harm_table)
        ae_length = len(adverse_event_table)
        trigger_counts_table_length = len(trigger_counts_table)
        trigger_type_counts_table_length = len(trigger_type_counts_table)

        # Now, build the PDF
        doc = SimpleDocTemplate(path, pagesize=letter, rightMargin=30, leftMargin=30,
                                topMargin=30, bottomMargin=18)
        doc.pagesize = portrait(letter)

        elements = []

        s = getSampleStyleSheet()
        nStyle = ParagraphStyle("centeredStyle", alignment=TA_CENTER, fontName='Helvetica-Bold', fontSize=12)
        # nStyle.fontsize = 12
        # nStyle.font = 'Helvetica-Bold'
        commentStyle = ParagraphStyle('commentStyle')
        commentStyle.fontSize = 8
        commentStyle.font = 'Helvetica'
        s = s['BodyText']
        s.wordWrap = 'CJK'

        trigger_counts_table = [[Paragraph(cell, s) for cell in row] for row in trigger_counts_table]
        trigger_type_counts_table = [[Paragraph(cell, s) for cell in row] for row in trigger_type_counts_table]
        trigger_table = [[Paragraph(cell, s) for cell in row] for row in trigger_table]
        harm_table = [[Paragraph(cell, s) for cell in row] for row in harm_table]
        adverse_event_table = [[Paragraph(cell, s) for cell in row] for row in adverse_event_table]
        word_count_table = [[Paragraph(cell, s) for cell in row] for row in word_count_table]

        # Set up the table titles
        trigger_counts_table_title = Paragraph(trigger_counts_table_title, nStyle)
        trigger_type_counts_table_title = Paragraph(trigger_type_counts_table_title, nStyle)
        trigger_table_title = Paragraph(trigger_table_title, nStyle)
        trigger_table_comment = Paragraph(trigger_table_comment, commentStyle)
        harm_table_title = Paragraph(harm_table_title, nStyle)
        adverse_event_table_title = Paragraph(adverse_event_table_title, nStyle)
        word_count_table_title = Paragraph(word_count_table_title, nStyle)

        ae_per_100 = Paragraph(self.ae_per_100, s)
        harmful_events_per100 = Paragraph(self.harm_per_100, s)
        outside_events = Paragraph(self.outside_events, s)

        trigger_counts_table = Table(trigger_counts_table, repeatRows=1)
        trigger_type_counts_table = Table(trigger_type_counts_table, repeatRows=1)
        trigger_table = Table(trigger_table, repeatRows=1)
        harm_table = Table(harm_table, repeatRows=1)
        adverse_event_table = Table(adverse_event_table, repeatRows=1)
        word_count_table = Table(word_count_table, repeatRows=1)

        defaultTableStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                        ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                        ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                        ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                        ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                        ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                        ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                        ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                        ('FONTNAME', (0, 0), (-1, -1), "Helvetica")
                                        ])

        # Alternating row colors
        if trigger_counts_table_length < 3:
            triggerCountsStyle = defaultTableStyle
        else:
            for each in range(trigger_counts_table_length):
                if each % 2 == 0:
                    bg_color = PdfRow1Color
                else:
                    bg_color = PdfRow2Color

                triggerCountsStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                                 ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                                 ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                                 ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                                 ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                                 ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                                 ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                                 ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                                 ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                                 ('BACKGROUND', (0, each), (-1, each), bg_color),
                                                 ('FONTNAME', (0, 0), (-1, -1), "Helvetica"),
                                                 ('KEEPTOGETHER', (0, 2), (-1, 3))
                                                 ])
                trigger_counts_table.setStyle(triggerCountsStyle)

        if trigger_type_counts_table_length < 3:
            triggerTypeCountsStyle = defaultTableStyle
        else:
            for each in range(trigger_type_counts_table_length):
                if each % 2 == 0:
                    bg_color = PdfRow1Color
                else:
                    bg_color = PdfRow2Color

                triggerTypeCountStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                                    ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                                    ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                                    ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                                    ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                                    ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                                    ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                                    ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                                    ('BACKGROUND', (0, each), (-1, each), bg_color),
                                                    ('FONTNAME', (0, 0), (-1, -1), "Helvetica"),
                                                    ('KEEPTOGETHER', (0, 2), (-1, 3))
                                                    ])
                trigger_type_counts_table.setStyle(triggerTypeCountStyle)

        if trigger_length < 3:
            triggerStyle = defaultTableStyle
        else:
            for each in range(trigger_length):
                if each % 2 == 0:
                    bg_color = PdfRow1Color
                else:
                    bg_color = PdfRow2Color

                triggerStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                           ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                           ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                           ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                           ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                           ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                           ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                           ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                           ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                           ('BACKGROUND', (0, each), (-1, each), bg_color),
                                           ('FONTNAME', (0, 0), (-1, -1), "Helvetica"),
                                           ('KEEPTOGETHER', (0, 2), (-1, 3))
                                           ])

                trigger_table.setStyle(triggerStyle)

        for each in range(harm_length):
            if each % 2 == 0:
                bg_color = PdfRow1Color
            else:
                bg_color = PdfRow2Color

            harmStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                    ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                    ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                    ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                    ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                    ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                    ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                    ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                    ('BACKGROUND', (0, each), (-1, each), bg_color),
                                    ('FONTNAME', (0, 0), (-1, -1), "Helvetica")
                                    ])

            harm_table.setStyle(harmStyle)

        for each in range(ae_length):
            if each % 2 == 0:
                bg_color = PdfRow1Color
            else:
                bg_color = PdfRow2Color

            aeStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                  ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                  ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                  ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                  ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                  ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                  ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                  ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                  ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                  ('BACKGROUND', (0, each), (-1, each), bg_color),
                                  ('FONTNAME', (0, 0), (-1, -1), "Helvetica")
                                  ])

            adverse_event_table.setStyle(aeStyle)

        if word_count_output:
            for each in range(word_table_length):
                if each % 2 == 0:
                    bg_color = PdfRow1Color
                else:
                    bg_color = PdfRow2Color

                wordTableStyle = TableStyle([('ALIGN', (1, 1), (-2, -2), 'RIGHT'),
                                             ('TEXTCOLOR', (1, 1), (-2, -2), colors.red),
                                             ('VALIGN', (0, 0), (0, -1), 'TOP'),
                                             ('TEXTCOLOR', (0, 0), (0, -1), colors.blue),
                                             ('ALIGN', (0, -1), (-1, -1), 'CENTER'),
                                             ('VALIGN', (0, -1), (-1, -1), 'MIDDLE'),
                                             ('TEXTCOLOR', (0, -1), (-1, -1), colors.green),
                                             ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                             ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                                             ('BACKGROUND', (0, each), (-1, each), bg_color),
                                             ('FONTNAME', (0, 0), (-1, -1), "Helvetica"),
                                             ('NOSPLIT', (-1, 0), (2, 3))
                                             ])

                word_count_table.setStyle(wordTableStyle)

        trigger_type_counts_table = KeepTogether([trigger_type_counts_table_title, Spacer(1, 0.1 * inch),
                                                  trigger_type_counts_table])

        trigger_table = KeepTogether([trigger_table_title, Spacer(1, 0.1 * inch), trigger_table])
        harm_table = KeepTogether([harm_table_title, Spacer(1, 0.1 * inch), harm_table])
        adverse_event_table = KeepTogether([adverse_event_table_title, Spacer(1, 0.1 * inch), adverse_event_table])
        if word_count_output:
            word_count_table = KeepTogether([word_count_table_title, Spacer(1, 0.1 * inch), word_count_table])

        # Add the tables to the PDF object
        elements.append(Spacer(1, 1 * inch))  # Spacer after header

        elements.append(trigger_counts_table_title)
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(trigger_counts_table)
        elements.append(Spacer(1, 0.2 * inch))

        # elements.append(trigger_type_counts_table_title)
        # elements.append(Spacer(1, 0.2 * inch))
        elements.append(trigger_type_counts_table)
        elements.append(Spacer(1, 0.2 * inch))

        # elements.append(trigger_table_title)
        #elements.append(Spacer(1, 0.1 * inch))
        elements.append(trigger_table)
        elements.append(Spacer(1, 0.1 * inch))
        elements.append(trigger_table_comment)
        elements.append(Spacer(1, 0.2 * inch))

        # elements.append(harm_table_title)
        #elements.append(Spacer(1, 0.1 * inch))
        elements.append(harm_table)
        elements.append(Spacer(1, 0.2 * inch))

        # elements.append(adverse_event_table_title)
        #elements.append(Spacer(1, 0.1 * inch))
        elements.append(adverse_event_table)
        elements.append(Spacer(1, 0.2 * inch))

        if word_count_output:
            # elements.append(word_count_table_title)
            # elements.append(Spacer(1, 0.1 * inch))
            elements.append(word_count_table)
            elements.append(Spacer(1, 0.2 * inch))

        elements.append(ae_per_100)
        elements.append(Spacer(.1, .1))

        elements.append(harmful_events_per100)
        elements.append(Spacer(.1, .1))

        elements.append(outside_events)
        elements.append(Spacer(.1, .1))
        # elements[] = KeepTogether(trigger_table)  # TODO: Enable this when display is fixed.

        # Write the PDF
        doc.build(elements, onFirstPage=pdfTitle, onLaterPages=pdfBody)

    def helpDisplay(self):
        """
        Opens the help PDF
        :return: GUI stuff
        """
        system = platform.system()
        print(system)
        try:
            base_path = sys._MEIPASS
            base_path = os.path.join(base_path, 'assets')
        except Exception:
            base_path = os.path.abspath('assets')

        pdfFile = os.path.join(base_path, 'GTT_Manual.pdf')

        # pdfFile = os.path.join('assets', 'GTT_Manual.pdf')
        # pdfFile = os.path.join(os.getcwd(), 'assets/GTT_Manual.pdf')
        print(pdfFile)
        if platform.system() == 'Windows':
            print("Windows!!!")
            subprocess.Popen([pdfFile], shell=True)
        elif platform.system() == 'Linux' or platform.system() == 'Darwin':
            os.system("open " + pdfFile)


# def popupAbout(self):
#        """
#        Brings up about dialog.
#        :return:
#        """
#        self.dialog = AboutPopUp()
#        self.app.exec()


# class AboutPopUp(Ui_about):
#    """
#    Pop-up dialog displaying information about the tool
#    """

#    def __init__(self, parent=None):
#        QtGui.QMainWindow.__init__(self)
#        Ui_about.__init__(self)
#        self.setupUi(self)


def pdfTitle(canvas, doc):
    """
    Write first page of PDF (title)
    :param canvas: reportlab canvas
    :param doc:
    :return:
    """

    title = 'Global Trigger Tool Report'
    date = datetime.datetime.now().strftime('%m/%d/%Y')  # ('%Y-%m-%d')
    width, height = letter
    canvas.saveState()
    canvas.setFont('Helvetica', 16)
    canvas.drawCentredString(width / 2, height - 50, title)  # Title
    canvas.setFont('Helvetica', 12)
    canvas.drawCentredString(width / 2, height - 75, date)  # Date
    canvas.restoreState()


def pdfBody(canvas, doc):
    """
    Write additional PDF pages
    :param canvas: reportlab canvas
    :param doc:
    :return:
    """
    date = datetime.datetime.now().strftime('%m/%d/%Y')
    pageinfo = 'Global Trigger Tool Report - {0}'.format(date)

    canvas.saveState()
    canvas.setFont('Helvetica', 8)
    canvas.drawString(inch, 10, "Page {0} {1}".format(doc.page, pageinfo))
    canvas.restoreState()


def SheetLister(path):
    """
    function scans directory and makes a list of spreadsheets
    :param path: user input string denoting path to directory
    :return: returns list of file names only including desired filetype
    """
    FileList = []

    for root, _, files in os.walk(path):  # Search through all files and directories within path

        for file in files:
            if file.endswith('.xlsx') or file.endswith('.csv') or file.endswith('.xlsm') or file.endswith(".xls") \
                    and '~$' not in file and "template" not in file:
                filePath = os.path.join(root, file)
                FileList.append(filePath)  # add file to the list

    if len(FileList) > 0:
        valid_path = True

    return FileList


def check_ae_type(ae_type):
    """
    Checks entered adverse event types against official AE type list in config/dictionaries
    :param ae_type: a string denoting the adverse event type to check
    :return: bool
    """
    abbr_ae_type_list = ['I', 'M', 'E', 'F', 'L/D', 'O', 'P']

    try:
        valid_AEtypes = config['Adverse Events']
    except:  # Loading config file didn't work - use default values
        valid_AEtypes = list(
            valid_ae_value.keys())  # Create a list of valid adverse event types from dictionary keys

    ae_type = ae_type.replace(" ", "")

    if len(ae_type) == 0:
        return True
    elif ae_type.title() in NA_list:
        return True
    elif ae_type in valid_AEtypes or ae_type in abbr_ae_type_list:
        return True
    else:
        return False

def check_harm_score(harm_score):
    """
    Checks entered harm score against official harm score category list in config/dictionaries
    :param harm_score: a character denoting the harm score type to check
    :return: bool
    """

    try:
        valid_harm_scores = config['Harm Categories']
    except:  # Loading config file didn't work - use default values
        valid_harm_scores = list(
            valid_harm_score_value.keys())  # Create a list of valid harm scores from dictionary keys

    harm_score = harm_score.replace(" ", "")

    if len(harm_score) == 0:
        return True
    elif harm_score.upper() in NA_list:
        return True
    elif harm_score in valid_harm_scores:
        return True
    else:
        return False


def check_triggers(trigger):
    """
    Takes a list of triggers from data source and checks it against valid triggers list in config/dictionaries
    :param trigger: key that describes types of high risk events
    :return:
    """
    is_valid_trigger = True
    is_ind_trigger_with_harm = False
    invalid_trigger = None

    try:
        valid_triggers = config['Valid Triggers']
    except NameError:
        valid_triggers = valid_trigger_value.keys()

    for i in trigger:
        if is_valid_trigger == True:
            i = i.replace(" ", "")

            if len(i) == 0 or i.upper() in NA_list or i in valid_triggers and len(i) <= 3:
                is_valid_trigger = True

            else:
                print('invalid trigger: {0}'.format(i))
                print('invalid trigger length: {0}'.format(len(i)))
                print('trigger is in valid triggers list: {0}'.format(i in valid_triggers))
                print('trigger is in NA_LIST: {0}'.format(i in NA_list))
                is_valid_trigger = False
                invalid_trigger = i.upper()
        else:
            break

    if len(trigger) == 1 and is_valid_trigger == True:
        is_ind_trigger_with_harm = True

    return is_valid_trigger, is_ind_trigger_with_harm, invalid_trigger


def ae_distr_table(adverse_events_per_row):
    """
    Calculates the percentage of each adverse event type category out of all adverse events
    (adverse events assigned to particular AE type category / all adverse events) * 100
    :param adverse_events_per_row: list of lists of adverse events
    :return: print results to screen
    """

    ae_types = {}
    final_list = []

    for i in adverse_events_per_row:
        ae_type_count = adverse_events_per_row.count(i)
        ae_type_percent = (ae_type_count / len(adverse_events_per_row) * 100)

        temp_list = (ae_type_count, ae_type_percent)

        ae_types[i] = temp_list

    for k, v in ae_types.items():
        temp_list = (k, v[0], v[1])
        final_list.append(temp_list)

    final_list.sort(key=lambda x: float(x[2]), reverse=True)

    return final_list


def harm_categ_distr_table(harm_categories_per_row):
    """
    Calculates the percentage of each harm category out of all harm categories
    (adverse events assigned to a particular harm category / all adverse events) * 100
    :param harm_categories_per_row: list of lists of harm categories
    :return: print results to screen
    """
    harm_categs = {}
    final_list = []
    for i in harm_categories_per_row:
        harm_category_count = harm_categories_per_row.count(i)
        harm_categ_percent = (harm_category_count / len(harm_categories_per_row) * 100)

        temp_list = (harm_category_count, harm_categ_percent)
        harm_categs[i] = temp_list

    for k, v in harm_categs.items():
        temp_list = (k, v[0], v[1])

        final_list.append(temp_list)

    final_list.sort(key=lambda x: float(x[2]), reverse=True)

    return final_list


def tppv(total_triggers, ind_triggers_with_harm, test):
    """
    calculates trigger-positive predictive value = (# of times independent trigger is associated with harm /
    # of times that trigger was used) * 100
    :param total_triggers: list of triggers with and without associated adverse events
    :param ind_triggers_with_harm: list of independent triggers that are tied to a a harm score
    :return: double denoting tppv
    """

    triggers = {}
    combined_ind_trig_with_harm = []
    temp_list = []
    final_list = []
    counter = 1
    trigger_count = 0

    # Get count each trigger appears in ind_trigger_with_harm list...
    for row_list in ind_triggers_with_harm:
        # status_text = "Processing row {0} of {1}".format(counter, len(ind_triggers_with_harm) + 1)
        # print(status_text)
        row_ind_trig_with_harm = row_list[0]
        combined_ind_trig_with_harm.append(row_ind_trig_with_harm)
        count = combined_ind_trig_with_harm.count(row_ind_trig_with_harm)
        total_count = total_triggers.count(row_ind_trig_with_harm)

        triggers[row_ind_trig_with_harm] = [((float(count) / float(total_count)) * 100), count]

        counter += 1

    for trigger, stats in triggers.items():
        trigger = u'{}'.format(trigger)
        count = stats[1]
        percentage = stats[0]
        trigger_count += 1
        temp_list = (trigger, count, percentage)

        final_list.append(temp_list)

    if not test:
        final_list.sort(key=lambda x: float(x[2]), reverse=True)
        results = final_list

    else:
        final_list.sort(key=lambda x: str(x[0]), reverse=False)
        results = final_list

    return results


def find_column(sheet, cName):
    """
    Find column index by name
    :param cName: name to search for (string)
    :return: integer denoting column index
    """

    for row_index, col_index in product(range(sheet.nrows), range(sheet.ncols)):
        if sheet.cell(row_index, col_index).value == cName:
            return col_index, row_index


def ae_to_admission_rate(all_adverse_events, total_admissions):
    rate = (all_adverse_events / total_admissions) * 100

    rate = round(rate, 2)

    return rate


def harm_to_admission_rate(all_harm_events, total_admissions):
    rate = (all_harm_events / total_admissions) * 100

    rate = round(rate, 2)

    return rate


def preadmit_event_rate(outside_event_count, total_admissions):
    rate = (outside_event_count / total_admissions) * 100

    rate = round(rate, 2)

    return rate


def all_trigger_count(list_of_triggers):
    """
    Gets count of all triggers (regardless of with or without harm association)
    :param: list_of_triggers: list of all triggers to be counted
    :return:
    """

    trigger_counts = []
    trigger_upper_list = []
    trigger_types_list = []
    trigger_type_counts = []
    _temp_list = []

    # Make everything uppercase for matching
    for trigger in list_of_triggers:
        if trigger not in NA_list:
            trigger_types_list.append(trigger[0].upper())
            trigger_upper_list.append(trigger.upper())

    for trigger_type in trigger_types_list:

        # Rename trigger type to make the output prettier
        if trigger_type == 'C':
            trigger_type = 'Cares'
        if trigger_type == 'M':
            trigger_type = 'Medication'
        if trigger_type == 'S':
            trigger_type = 'Surgical'
        if trigger_type == 'P':
            trigger_type = 'Perinatal'
        if trigger_type == 'I':
            trigger_type = 'Intensive Care'
        if trigger_type == 'E':
            trigger_type = 'Emergency Department'
        _temp_list.append(trigger_type)

    trigger_types_list = _temp_list

    for trigger_type in trigger_types_list:
        type_count = trigger_types_list.count(trigger_type)
        type_percentage = (type_count / len(trigger_types_list) * 100)
        temp_trigger_type_tuple = (trigger_type, type_count, type_percentage)
        trigger_type_counts.append(temp_trigger_type_tuple)

    trigger_type_counts = list(set(trigger_type_counts))
    trigger_type_counts.sort(key=lambda x: float(x[2]), reverse=True)

    for trigger in trigger_upper_list:
        trigger_count = trigger_upper_list.count(trigger)
        trigger_percentage = (trigger_count / (len(list_of_triggers)) * 100)
        temp_trigger_tuple = (trigger, trigger_count, trigger_percentage)
        trigger_counts.append(temp_trigger_tuple)

    trigger_counts = list(set(trigger_counts))
    trigger_counts.sort(key=lambda x: float(x[2]), reverse=True)

    return trigger_counts, trigger_type_counts


if __name__ == '__main__':
    GTT.internal_config = True
    app = QtGui.QApplication(sys.argv)
    window = GTT(testMode=False)  # This disables Qt so that tests can run
    window.show()
    sys.exit(app.exec_())
