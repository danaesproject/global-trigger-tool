import hashlib
from unittest import TestCase

from gtt import *

current_path = os.path.dirname(os.path.abspath(__file__))

class TestGTT(TestCase):
    def test_word_count(self):
        """
        Tests word_count function.
        :return:
        """

        gtt = GTT(testMode=True)

        words = ['This is a sentence']
        expected_wordcount = [('This Is A Sentence', 1)]
        word_counts = gtt.word_count(words)
        self.assertEqual(expected_wordcount, word_counts)

    def test_check_if_config_exists(self):
        configPath = os.path.join(current_path, 'golden_config.txt')
        self.assertTrue(GTT.check_if_config_exists(GTT, configPath))

    def test_readConfig(self):
        """
        Test configuration file by making sure a test file is being read correctly.
        :return:
        """
        GTT.testMode = True
        configPath = os.path.join(current_path, 'golden_config.txt')
        GTT.configPath = configPath
        expected_results = {'Valid Triggers': ['C1', 'C10', 'C11', 'C12', 'C13', 'C14', 'C15', 'C2', 'C3', 'C4', 'C5',
                                               'C6', 'C7', 'C8', 'C9', 'E1', 'E2', 'I1', 'I2', 'I3', 'I4', 'M1', 'M10',
                                               'M11', 'M12', 'M13', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9',
                                               'P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'S1', 'S10', 'S11',
                                               'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8', 'S9'],
                            'Adverse Events': ['Equipment', 'Falls', 'Infection', 'Lab/Diagnostic', 'Medication',
                                               'Other', 'Procedural'],
                            'Harm Categories': ['E', 'F', 'G', 'H', 'I'],
                            'Header Row': '2',
                            'Triggers (With AE) Column Name': 'Trigger(s) WITH associated AE',
                            'Triggers (Without AE) Column Name': 'Trigger(s) WITHOUT associated AE',
                            'Harm Category Column Name': 'Harm Category (if AE is harmful)',
                            'Adverse Event Type Column Name': 'Adverse Event Type',
                            'Outside Event Column Name': 'Outside Event Y or N',
                            'Adverse Event Free Text Column': 'Adverse Event (AE)'}

        results = GTT.readConfig(GTT)
        self.assertEqual(results, expected_results)

    def test_createConfig(self):
        """
        Tests creation of config file by creating file and testing hash against a known good config
        :return:
        """
        GTT.testMode = True
        configPath = os.path.join(current_path, 'test_config.txt')
        GTT.configPath = configPath
        GTT.createConfig(GTT)

        with open(os.path.join(current_path, 'golden_config.txt')) as goldenfile:
            data = goldenfile.read().encode('utf-8')
            golden_hash = hashlib.md5(data).hexdigest()

        with open(os.path.join(current_path, 'test_config.txt')) as testfile:
            data = testfile.read().encode('utf-8')
            test_hash = hashlib.md5(data).hexdigest()
        os.remove(os.path.join(current_path, 'test_config.txt'))

        self.assertEqual(golden_hash, test_hash)

    def test_check_all_files(self):
        """
        Make sure this catches the duplicate entry in testData_2.xls
        :return:
        """
        gtt = GTT(testMode=True)
        GTT.testMode = True
        file_list = [os.path.join(current_path, 'testData_2.xls')]

        results = gtt.check_all_files(current_path, file_list, True)

        assert results is False

    def test_importer(self):
        """
        Test import of valid xlsx file
        :return:
        """

        gtt = GTT(testMode=True)
        file_list = [os.path.join(current_path, 'testData_1.xls')]

        # ΩExpected values follow
        test_total_triggers = ['C14', 'M13', 'C15', 'C4', 'S10', 'M10', 'M13', 'M10', 'M11', 'M12', 'S11', 'C13', 'S11',
                               'C14', 'C12']
        test_ind_triggers_with_harm = [['C14'], ['C15'], ['S10'], ['M13'], ['M10'], ['M11'], ['M12'], ['S11'], ['C13'],
                                       ['C14']]
        test_harm_categ = ['I', 'E', 'F', 'F', 'I', 'G', 'E', 'F', 'F', 'E']
        test_ae_type = ['Other', 'Infection', 'Infection', 'Other', 'Lab/Diagnostic', 'Medication', 'Infection',
                        'Infection',
                        'Infection', 'Medication']
        test_error_status = False

        # Run Test 1
        total_triggers, ind_triggers_with_harm, harm_categ, ae_type, error_status, total_admissions, \
        all_adverse_events, all_harm_events, outside_event_count, adverse_events_freetxt = gtt.importer(file_list)

        assert test_total_triggers == total_triggers
        assert test_ind_triggers_with_harm == ind_triggers_with_harm
        assert test_harm_categ == harm_categ
        assert test_ae_type == ae_type
        assert test_error_status is False
        assert total_admissions == 10
        assert all_adverse_events == 10
        assert all_harm_events == 10
        assert outside_event_count == 5
        assert adverse_events_freetxt == ['testing some freetext']

    def test_sheetLister(self):
        """
        Tests sheetLister function
        :return:
        """

        path = os.path.join(current_path)

        expected_results = [os.path.join(current_path, 'testData_1.xls'), os.path.join(current_path, 'testData_2.xls')]
        sheetLister_results = SheetLister(path)
        print(sheetLister_results)
        self.assertEqual(sheetLister_results, expected_results)

    def test_check_triggers_1(self):
        """
        Make sure trigger checker fails on first invalid trigger
        :return:
        """

        file_list = ['testData_1.xls']

        # Expected values

        triggers_to_check = ['A1', 'A2', 'b3']

        results = check_triggers(triggers_to_check)
        expected_results = (False, False, 'A1')

        self.assertEqual(results, expected_results)

    def test_check_triggers_2(self):
        """
        Make sure trigger checker passes on valid triggers, but states that it isn't an individual trigger
        :return:
        """

        file_list = ['testData_1.xls']

        # Expected values

        triggers_to_check = ['S1', 'M2', 'P1']

        results = check_triggers(triggers_to_check)
        expected_results = (True, False, None)

        self.assertEqual(results, expected_results)

    def test_check_triggers_3(self):
        """
        Make sure trigger checker passes on valid triggers, and states that it is an individual trigger
        :return:
        """

        file_list = ['testData_1.xls']

        # Expected values

        triggers_to_check = ['S1']

        results = check_triggers(triggers_to_check)
        expected_results = (True, True, None)

        self.assertEqual(results, expected_results)

    def test_find_column(self):
        """
        Tests that tool can correctly identify the column location
        :return:
        """
        spreadsheetPath = os.path.join(current_path, 'testData_1.xls')
        spreadsheet = xlrd.open_workbook(spreadsheetPath)  # identifies desired spreadsheet
        sheet = spreadsheet.sheet_by_index(0)  # specifies sheet to open within spreadsheet

        colIndex, rowIndex = find_column(sheet, 'Adverse Event Type')
        results = (colIndex, rowIndex)
        expected_results = (12, 1)

        self.assertEqual(results, expected_results)

    def test_ae_to_admission_rate(self):
        """
        Tests output of ae_to_admission_rate
        :return:
        """

        expected_rate = 10.00
        rate = ae_to_admission_rate(100, 1000)
        self.assertEqual(rate, expected_rate)

    def test_harm_to_admission_rate(self):
        """
        Tests output of ae_to_admission_rate
        :return:
        """

        expected_rate = 10.00
        rate = harm_to_admission_rate(100, 1000)
        self.assertEqual(rate, expected_rate)

    def test_preadmit_event_rate(self):
        """
        Tests output of ae_to_admission_rate
        :return:
        """
        expected_rate = 10.00
        rate = preadmit_event_rate(100, 1000)
        self.assertEqual(rate, expected_rate)

    def test_all_trigger_count(self):
        """
        Test that the all_trigger_count function is working
        :return:
        """

        triggers = ['A1', 'A1', 'A1', 'P3', 'p3', "Z2"]
        expected_trigger_counts = [('A1', 3, 50.0), ('P3', 2, 33.33333333333333), ('Z2', 1, 16.666666666666664)]
        trigger_counts, _ = all_trigger_count(triggers)

        self.assertEqual(expected_trigger_counts, trigger_counts)

    def test_all_trigger_type_count(self):
        """
        Test that the all_trigger_count function is working
        :return:
        """

        triggers = ['C1', 'C1', 'C1', 'P3', 'p3', "I2"]
        expected_trigger_type_counts = [('Cares', 3, 50.0), ('Perinatal', 2, 33.33333333333333),
                                        ('Intensive Care', 1, 16.666666666666664)]
        _, trigger_type_count = all_trigger_count(triggers)

        self.assertEqual(expected_trigger_type_counts, trigger_type_count)

    def test_tppv(self):
        """
        Test the TPPV function
        :return:
        """

        total_triggers = ['C1', 'C3', 'P1', 'P2', 'I1', 'I1', 'C3', 'A7', 'A7', 'A7', 'A7']
        ind_trig_with_harm = [['C1'], ['C3'], ['P1'], ['A7']]
        expected_results = [('A7', 1, 25.0), ('C1', 1, 100.0), ('C3', 1, 50.0), ('P1', 1, 100.0)]

        tppv_results = tppv(total_triggers, ind_trig_with_harm, True)
        print(tppv_results)

        self.assertEqual(expected_results, tppv_results)

    def test_harm_cat_distr_table(self):
        """
        Test the harm category distribution table code
        :return:
        """
        expected_results = [('H', 3, 50.0), ('I', 2, 33.33333333333333), ('F', 1, 16.666666666666664)]

        harm_cat_per_row = ['H', 'H', 'H', 'I', 'I', 'F']
        hc_results = harm_categ_distr_table(harm_cat_per_row)
        print(harm_cat_per_row)
        self.assertEqual(expected_results, hc_results)

    def test_ae_distr_table(self):
        """
        Test adverse events distribution table code
        :return:
        """
        expected_results = [('Falls', 3, 50.0), ('Infection', 2, 33.33333333333333),
                            ('Lab/Diagnostic', 1, 16.666666666666664)]
        ae_per_row = ['Falls', 'Falls', 'Falls', 'Infection', 'Infection', 'Lab/Diagnostic']
        ae_results = ae_distr_table(ae_per_row)

        print(ae_results)
        self.assertEqual(expected_results, ae_results)
