# -*- mode: python -*-

block_cipher = None


a = Analysis(['gtt.py'],
             pathex=['Z:\\home\\rwardrup\\bamboo-home\\xml-data\\build-dir\\GTTA-NEB-JOB1'],
             binaries=[],
             datas=[('assets\\GTT_Manual.pdf', 'assets')],
             hiddenimports=['xlrd'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          #a.binaries,
		  exclude_binaries=True,
          #a.zipfiles,
          #a.datas,
          name='gtt',
          debug=True,
          strip=False,
          upx=False,
          console=True, icon='assets\\icon.ico' )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='gtt-debug')
