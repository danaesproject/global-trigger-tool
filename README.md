# Global Trigger Tool #


[![Build Status](http://ci.imspatial.me/buildStatus/icon?job=GTTA/master)](http://ci.imspatial.me/job/GTTA/job/master/)

---

The Global Trigger Tool (GTT) is a Windows/Mac/Linux Python tool that compiles data from all .csv, .xls, .xlsx, and .xlsm files found within a user-specified directory and produces several tables:

* Adverse Event Type Distribution Table
* Trigger-Positive Predictive Value Table
* Harm Category Distribution Table

GTT allows saving to a .txt file or printing to the default printer. The printing functionality requires that a default printer is set.

### What is this repository for? ###

* Danae Wardrup's JPS internship project
* Current version: 0.1-Alpha

### How do I get set up? ###

* Requires Python 3.5.
* Change directory to global-trigger-tool, and run "pip3 install -r requirements.txt" to install the required modules.
* To run unittests, install the "nose" package via pip.
* If you'd like to compile the code to a Windows executable, install pyinstaller and then run the build.bat file within Windows (simply double-click it).

### Who do I talk to? ###

* Danae Wardrup - Developer