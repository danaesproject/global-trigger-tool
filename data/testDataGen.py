"""
Generate test data for use in gtt.py
"""

import os
import random
import shutil

import xlrd
from xlutils.copy import copy as xlsCopy

from config import dictionaries as valid_entries

template_file = "template/template.xlsm"
created_files = "."

valid_ae_value = valid_entries.ae_type_category_dictionary
valid_harm_score_value = valid_entries.ae_harm_dictionary
valid_trigger_value = valid_entries.trigger_dictionary

valid_ae_value = list(valid_ae_value.keys())
valid_harm_score_value = list(valid_harm_score_value.keys())
valid_trigger_value = list(valid_trigger_value.keys())

# Add a blank entry to valid entries to allow for empty/null cells
valid_trigger_value.append(" ")
valid_trigger_value.append("N/A")

triggers_with_ae_row = 8
harm_category_row = 11
ae_type_row = 12
triggers_without_ae_row = 15

startDate = "1/1/2016"
endDate = "3/1/2016"

yn_choices = ["Y", "y", "N", "n", "Yes", "yes", "No", "no"]


def copy_template(filepath, n_spreadsheets):
    """
    Import the template xlsm file used to generate the sample data and create copies
    :param filepath: path to template file
    :return:
    """

    # Build a list of xlsm files
    filelist = [f for f in os.listdir(".") if f.endswith("xls")]

    # Remove xlsm files
    for f in filelist:
        os.remove(f)

    # Copy new files from template
    for i in range(n_spreadsheets):
        output_filename = "sampleData_{0}.xls".format(i)
        shutil.copyfile(filepath, output_filename)


def open_sample_files(sampleFilePath):
    """
    Opens the sample files, one-by-one
    :param sampleFilePath: String, path to sample files
    :return:
    """

    filelist = [f for f in os.listdir(".") if f.endswith("xls")]
    spreadsheets = {}

    for file in filelist:
        #_path = created_files + file

        spreadsheet = xlrd.open_workbook(file)  # identifies desired spreadsheet
        sheet = spreadsheet.sheet_by_index(0)  # specifies sheet to open within spreadsheet
        wb = xlsCopy(spreadsheet)
        ws = wb.get_sheet(0)
        spreadsheets[file] = [wb, ws]

    return spreadsheets


def write_to_spreadsheets(sheetDict, nRows):
    """
    Write values to spreadsheets
    :param sheetList: list of xlrd spreadsheets
    :return: file output
    """
    counter = 0
    harm_category = ''
    row_count = 1

    for filename, workbook in sheetDict.items():
        wb = workbook[0]
        ws = workbook[1]
        for i in range(nRows):  # Write 25 rows
            print("Writing row {0}.".format(row_count))
            row_count += 1
            if i > 1:
                # Pick a random value from valid triggers
                trigger_with_ae = random.choice(valid_trigger_value)

                # prevent duplication
                _valid_trigger_value = valid_trigger_value[:]
                _valid_trigger_value.remove(trigger_with_ae)
                if len(_valid_trigger_value) == 0:
                    input(valid_trigger_value)

                trigger_without_ae = random.choice(_valid_trigger_value)

                ae_type = random.choice(valid_ae_value)
                if len(ae_type) > 0:
                    harm_category = random.choice(valid_harm_score_value)

                # Pick a random date
                start_month = random.randint(1, 12)
                end_month = start_month + random.randint(0, 2)
                start_day = random.randint(1, 29)
                end_day = start_day + random.randint(1, 14)
                year = 2016

                start_date = "{0}/{1}/{2}".format(start_month, start_day, year)
                end_date = "{0}/{1}/{2}".format(end_month, end_day, year)

                ws.write(i, 0, i)  # Record number
                ws.write(i, 1, random.randint(0, 100000))  # MRN
                ws.write(i, 2, start_date)  # Admit Date
                ws.write(i, 3, end_date)  # Discharge Date
                ws.write(i, 4, random.randint(0, 25))  # LOS
                ws.write(i, 5, random.choice(yn_choices))  # Outside event
                ws.write(i, 6, random.choice(yn_choices))  # Readmit

                ws.write(i, triggers_with_ae_row, trigger_with_ae)
                ws.write(i, harm_category_row, harm_category)
                ws.write(i, ae_type_row, ae_type)
                ws.write(i, triggers_without_ae_row, trigger_without_ae)

        wb.save(filename)


if __name__ == '__main__':
    """
    Run the script
    """

    n_spreadsheets = int(input("How many spreadsheets should I create?: "))
    nRows = int(input("How many rows per spreadsheet?:"))

    copy_template(template_file, n_spreadsheets)

    spreadsheets = open_sample_files(created_files)

    write_to_spreadsheets(spreadsheets, nRows)
